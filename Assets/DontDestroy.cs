using System.Collections;
using System.Collections.Generic;
using DevToDev.Analytics;
using UnityEngine;

public class DontDestroy : MonoBehaviour
{
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
    }
    
    void Start()
    {
            var config = new DTDAnalyticsConfiguration
            {
                    ApplicationVersion = "1.2.3",
                    LogLevel = DTDLogLevel.Debug,
                    TrackingAvailability = DTDTrackingStatus.Enable,
                    CurrentLevel = 1,
                    UserId = "unique_userId"
            };
            
            Debug.Log("Started");
#if UNITY_ANDROID
        DTDAnalytics.Initialize("androidAppID");
#elif UNITY_IOS
        DTDAnalytics.Initialize("IosAppID");
#elif UNITY_WEBGL
        DTDAnalytics.Initialize("WebAppID");
#elif UNITY_STANDALONE_WIN
            
        DTDAnalytics.Initialize("6d6224a5-108e-0f3e-8392-165048366f5b", config);
#elif UNITY_STANDALONE_OSX
        DTDAnalytics.Initialize("OsxAppID");
#elif UNITY_WSA
        DTDAnalytics.Initialize("UwpAppID");
#endif
    }
}
